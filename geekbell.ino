#include <Entropy.h>
#include <SPI.h>
#include <SD.h>
#include "rtttl.h"

/*
   Pins 4 and 10 are used by Ethernet Shield!
*/
const byte bell = 5;  //Bell pin, must has PWM
const byte button = 8;  //Button pin, digital input

/*
  bool playing = false;

  byte song;
  unsigned long song_length = 1000;

  bool previous;
  unsigned long time = 0;
  const unsigned long debounce = 200;
*/

RamPlayer player(bell);
File root;
unsigned long files;

void setup() {
  Entropy.initialize();

  /*
    pinMode(LED_BUILTIN, OUTPUT);
    pinMode(button, INPUT_PULLUP);
    pinMode(bell, OUTPUT);
  */

  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  Serial.print("Initializing SD card...");

  if (!SD.begin(4)) {
    Serial.println("initialization failed!");
    return;
  }
  Serial.println("initialization done.");

  root = SD.open("/");

  files = count();
  Serial.println(files);
}

void loop() {
  Serial.println(randomMelody());
  player.finishSong();
}

unsigned long count() {
  unsigned long count = 0;
  while (true) {
    File f = root.openNextFile();
    if (f) {
      f.close();
      count++;
    } else {
      f.close();
      break;
    }
  }
  root.rewindDirectory();
  return count;
}

String randomMelody() {
  root.rewindDirectory();
  File file;
  for (long i = Entropy.random(files); i >= 0 ; i--) {
    //Serial.println(i);
    file.close();
    file = root.openNextFile();
  }
  Serial.println(file.name());

  String melody;
  while (file.available()) {
    char c = file.read();
    //Serial.print(c);
    melody += c;
  }
  file.close();
  root.rewindDirectory();
  return melody;
}

